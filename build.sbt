name := "Embedded Cassandra plugin for SBT"

normalizedName := "cunit"

organization := "com.filez.scala.sbt"

version := "0.2.0"

val Versions = new { 
    val CassandraUnit = "4.3.1.0"
    val EmbeddedCassandra = "4.0.3"
    val Cassandra = "3.11.6"
    val CQLDriver = "3.7.2"
    val java8compat = "1.0.0-RC1"
}

sbtPlugin := true

resolvers += Resolver.mavenLocal

libraryDependencies += "com.github.nosan" % "embedded-cassandra" % Versions.EmbeddedCassandra
libraryDependencies += "org.scala-lang.modules" %% "scala-java8-compat" % Versions.java8compat

/*
libraryDependencies ++= Seq(
    "org.cassandraunit" % "cassandra-unit" % Versions.CassandraUnit,
    "com.datastax.cassandra" % "cassandra-driver-core" % Versions.CQLDriver,
    "org.apache.cassandra" % "cassandra-all" % Versions.Cassandra
)
*/

publishMavenStyle := true

Test / publishArtifact := false

pomIncludeRepository := { _ => false }

publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases"  at nexus + "service/local/staging/deploy/maven2")
}

organizationName := "Filez.com"

description := "Launches Cassandra for unit tests"

startYear := Some(2018)

versionScheme := Some("early-semver")

homepage := Some(url("https://gitlab.com/hsn10/cunit-sbt"))

licenses := Seq("MIT" -> url("https://www.gnu.org/licenses/gpl-2.0.html"))

developers := List(Developer("hsn10", "Radim Kolar", null, null))

scmInfo := Some(ScmInfo(url("https://gitlab.com/hsn10/cunit-sbt"),
 "scm:git:https://gitlab.com/hsn10/cunit-sbt.git", Some("git@gitlab.com:hsn10/cunit-sbt.git")))
