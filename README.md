## Cassandra Unit Plugin for SBT

This plugin starts embedded Cassandra 3.11.6 for use in sbt unit tests.
Starts CQL on port 9142.

Added SBT tasks:
* startCassandra - Starts embedded Cassandra
* stopCassandra - Stops embedded Cassandra
* restartCassandra - Restarts embedded Cassandra
* cleanCassandra - Drops Cassandra keyspaces

Cassandra is automatically started for tests.

Plugin is in Maven Central. You can also clone this repository and run *sbt publishLocal*

Then enable plugin in *project/plugins.sbt*

addSbtPlugin("com.filez.scala.sbt" %% "cunit" % "0.1.5")

### Versions
1. Cassandra 3.X / Java 8 - use 0.1.5
1. Cassandra 4.X / Java 11 - use 0.2.0
