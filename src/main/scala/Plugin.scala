package com.filez.scala.sbt.cunit

import sbt._
import Keys._

import com.github.nosan.embedded.cassandra.{CassandraBuilder, WorkingDirectoryDestroyer, SimpleSeedProviderConfigurator}
import com.github.nosan.embedded.cassandra.Cassandra
import com.github.nosan.embedded.cassandra.commons.ClassPathResource
import scala.compat.java8.FunctionConverters._
import java.util.concurrent.atomic.AtomicReference


/**
 * Cassandra Unit SBT Plugin.
 *
 * This plugin starts embedded Cassandra for unit tests.
 * It starts CQL on port 9142.
 */
object CassandraUnitPlugin extends AutoPlugin {
   override def requires: Plugins = sbt.plugins.JvmPlugin
   override def trigger = allRequirements
   object autoImport {
      val startCassandra = taskKey[Unit]("Starts embedded Cassandra")
      val stopCassandra  = taskKey[Unit]("Stops embedded Cassandra")
     val restartCassandra= taskKey[Unit]("Restarts embedded Cassandra")
   }
   import autoImport._
   val reloadHook = (s: State) => { if (instance.get() != null && instance.get().isRunning()) { instance.get().stop();s } else s }
   override lazy val projectSettings: Seq[Def.Setting[_]] = Seq(
       startCassandra := startCassandraTask.value,
       stopCassandra := stopCassandraTask.value,
       restartCassandra := restartCassandraTask.value,

       Test / test := (Test / test).dependsOn(startCassandra).value,
 //      stopCassandra := stopCassandra.dependsOn(startCassandra).value,
       fork := true,
//     reload hook
       onUnload in Global ~= (reloadHook compose _)
   )
   val instance : AtomicReference[Cassandra] = new AtomicReference()

   lazy val startCassandraTask = Def.task {
       initInstance()
       val out = streams.value.log
       if (instance.get().isRunning() == false) {
          try {            
             out.info(**("CASSANDRA STARTING"))
             instance.get().start()
          } catch {
             case e:Exception => 
                out.info(**("!!! cassandra start failed"))
          }
       }
   }

   lazy val restartCassandraTask = Def.task {
       streams.value.log.info(**("RESTARTING CASSANDRA"))
       if (instance.get() != null && instance.get().isRunning()) {
          try {
               instance.get().stop()
          } catch {
          case _:Exception =>
          } 
       }
       initInstance()
       instance.get().start()
   }

   lazy val stopCassandraTask = Def.task {
       if (instance.get() != null && instance.get().isRunning()) {
          streams.value.log.info(**("CASSANDRA STOPPING"))
          instance.get().stop()
       }
   }

   private def **(text: String) = s"***** $text *****"

   private def initInstance() : Unit = {
       import java.nio.file.Paths
       // init instance
       instance.compareAndSet(null,
	       new CassandraBuilder()
                  .addWorkingDirectoryResource(new ClassPathResource("cassandra4.bat"),
                     "bin/cassandra.bat")
                  .workingDirectoryDestroyer(WorkingDirectoryDestroyer.doNothing())
                  .addConfigProperty("native_transport_port", 9142)
                  .addConfigProperty("storage_port", 7010)
                  .addConfigProperty("ssl_storage_port", 7011)
                  .workingDirectory(() => Paths.get("target/embedded"))
                  .configure(new SimpleSeedProviderConfigurator("localhost:7010"))
                  .build()
       )
   }
}